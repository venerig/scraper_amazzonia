# Amazzonia automation
![robotframework](https://img.shields.io/badge/powered%20by-robotframework-brightgreen) 

This is the repository for Automation related to Amazzonia project.

## Usage

To run, you can choose to `git clone`. 


## Available test variables

The only test variable are the Amazon product landing page. You can find it inside the .robot file.

## Running a specific file

You can run the scraper as single job with the following command: 
```
robot -d /Result -L info -b debug.log -t "Check Price Product list Green" amazzonia.robot
```
Or you can run the scraper as parallel suite (using Pabot) with the following command: 
```
pabot --testlevelsplit --processes 4 --outputdir /Result/Pabot amazzonia.robot 
```

## Test sections

There is only one .robot file inside the project, here the project structure:
```
Amazzonia/
    /amazzonia.robot
    /README
```
<!-- NB: A dummy file is left on the tests/ root to prevent testing all files on commit. -->
