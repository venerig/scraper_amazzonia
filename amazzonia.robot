*** Settings ***
Test Setup      Open Chrome Headless
Test Template   Navigator
Test Teardown   Close Browser
Default Tags    DEBUG
Test Timeout       3 minute   #this is a global timeout for test execution
Library  SeleniumLibrary
Library  String
Library  Collections

*** Variable ***
${timeout}  15s
${is_headless}  True
@{URL_LIST}
${max_price}  20

${url_first}  #put here your target Amazon url as variable
${url_second}  #put here your target Amazon url as variable
${url_third}  #put here your target Amazon url as variable

*** Test Case ***   URL_HOME	 COUNT
Check Price Product list Green	 ${url_sc_escu}            
Check Price Product list White   ${url_sc_barca}
Check Price Product list Blue    ${url_sc_stringate}

*** Keywords ***
Open Chrome Headless
	${options}    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
	${ws}=    Set Variable    window-size=1920,1080
	run keyword if    '${is_headless}' == 'True'    Call Method    ${options}    add_argument    --headless
	run keyword if    '${is_headless}' == 'True'    Call Method    ${options}    add_argument    --disable-gpu
	Call Method    ${options}    add_argument    ${ws}

	Create WebDriver    Chrome    chrome_options=${options}
	Maximize Browser Window

Navigator
	[Arguments]  ${url_trip}  ${count}=${EMPTY}
	Go to  ${url_trip}
	Wait For Condition  return document.readyState=="complete"  timeout=60s  error=None
	Wait Until Element Is Visible  xpath://*[@data-index="1"]  timeout=None  error=Captcha visible!
	${result_count}  Get Element Count  xpath://div[@data-component-id]
	${range}  Run Keyword If  '${count}' == '${EMPTY}'  Set Variable If  ${result_count} > 5  5  ${result_count}
  ...  ELSE  Set Variable  ${count}
		FOR  ${i}  IN RANGE  1  ${range}
			${product_view}  Set Variable  xpath://*[@data-index="${i}"]
			${price_whole}  Set Variable  xpath://*[@data-index="${i}"]//*[@class="a-price-whole"]
			${price_ship}  Set Variable  xpath://*[@data-index="${i}"]//*[contains(@aria-label,"di spedizione")]
		
			${is_net_price}  Run Keyword And Return Status  Wait Until Element Is Visible  ${price_whole}
			${net_price}  Run Keyword If  '${is_net_price}' == 'True'  Get Text  ${price_whole}
			...  ELSE  Set Variable  ${max_price},00
			${net_price}  Replace String  ${net_price}  ,  .  count=-1
			
			${is_ship_price}  Run Keyword And Return Status  Wait Until Element Is Visible  ${price_ship}  timeout=None  error=None
			${ship_price}  Run Keyword If  '${is_ship_price}' == 'True'  Get Text  ${price_ship}
			...  ELSE  Set Variable  0,0
			${ship_price}  Remove String  ${ship_price}	 € di spedizione
			${ship_price}  Replace String  ${ship_price}  ,  .  count=-1
			
			${is_under_twenty}  Run Keyword And Return Status  Should Be True  ${{ ${net_price} + ${ship_price} }} < ${max_price}  msg=Price over! ${{ ${net_price} + ${ship_price} }} < ${max_price}
			Log To Console  \n${is_under_twenty}: ${{ ${net_price} + ${ship_price} }} < ${max_price}  stream=STDOUT  no_newline=False
			Capture Element Screenshot  ${product_view}
			${current_url}  Run Keyword If  '${is_under_twenty}' == 'True'  Get Location
      ...  ELSE  Set Variable  ${EMPTY}
			Append To List  ${URL_LIST}    ${current_url}
			Run keyword And Continue On Failure  Run Keyword If  '${is_under_twenty}' == 'True'  Fail  msg=${URL_LIST}
		END